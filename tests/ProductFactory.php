<?php
use Easydev\Vendor\ShoppingCart\Cart\Products\ProductFactory;
use Easydev\Vendor\ShoppingCart\Cart\Products\Types\Lemon;

class ProductFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductFactory */
    protected $productFactory;

    public function setUp()
    {
        $this->productFactory = new ProductFactory();
    }

    public function testProductCreation()
    {
        $product = $this->productFactory->create('Lemon');
        $this->assertInstanceOf(Lemon::class, $product);
    }

    /**
     * @expectedException Easydev\Vendor\ShoppingCart\Cart\Products\Exceptions\InvalidProductException
     */
    public function testThrowsExceptionOnInvalidProduct()
    {
        $this->productFactory->create('AmazingFruit');
    }

}
