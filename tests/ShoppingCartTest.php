<?php
use Easydev\Vendor\ShoppingCart\Cart\Cart;
use Easydev\Vendor\ShoppingCart\Cart\Products\ProductFactory;

class ShoppingCartTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductFactory */
    protected $productFactory;

    public function setUp()
    {
        $this->productFactory = new ProductFactory();
    }

    public function testAddProductToCart()
    {
        $product = $this->productFactory->create('Lemon');
        $cart = new Cart();
        $cart->addProduct($product, 5);

        $this->assertEquals(count($cart->getProducts()), 5);
    }

    public function testAddMultipleProductsToCart()
    {
        $lemon = $this->productFactory->create('Lemon');
        $tomato = $this->productFactory->create('Tomato');
        $cart = new Cart();
        $cart->addProduct($lemon, 5);
        $cart->addProduct($tomato, 2);

        $this->assertEquals(count($cart->getProducts()), 7);
    }

}
