<?php
namespace Easydev\Vendor\ShoppingCart\Cart;

use Easydev\Vendor\ShoppingCart\Cart\Products\ProductsCollection;
use Easydev\Vendor\ShoppingCart\Cart\Products\ProductType;

class Cart {

    private $products;

    public function __construct()
    {
        $this->products = new ProductsCollection();
    }
    /**
     * @param ProductType $product
     * @param Int $amount
     */
    public function addProduct(ProductType $product, $amount)
    {
        $product->setAmount($amount);
        $this->products->add($product);
    }

    /**
     * @return ProductsCollection
     */
    public function getProducts()
    {
        return $this->products;
    }
}