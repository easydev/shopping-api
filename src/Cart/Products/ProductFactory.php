<?php
namespace Easydev\Vendor\ShoppingCart\Cart\Products;

use Easydev\Vendor\ShoppingCart\Cart\Products\Exceptions\InvalidProductException;
use Easydev\Vendor\ShoppingCart\Cart\Products\Types;

class ProductFactory
{
    /**
     * @param $product
     * @return mixed
     * @throws InvalidProductException
     */
    public static function create($product)
    {
        $classNamespace = 'Easydev\Vendor\ShoppingCart\Cart\Products\Types\\'.ucfirst($product);
        if (class_exists($classNamespace)) {
            return new $classNamespace;
        } else {
            throw new InvalidProductException("Product doesn't exist.");
        }
    }
}