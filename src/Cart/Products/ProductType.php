<?php
namespace Easydev\Vendor\ShoppingCart\Cart\Products;

abstract class ProductType
{
    private $amount;

    /**
     * @return string
     */
    public function getName()
    {
        return self::PRODUCT_NAME;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param Int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

}
