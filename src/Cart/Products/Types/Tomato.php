<?php
namespace Easydev\Vendor\ShoppingCart\Cart\Products\Types;

use Easydev\Vendor\ShoppingCart\Cart\Products\ProductType;

class Tomato extends ProductType
{
    const PRODUCT_NAME = 'Tomato';

}