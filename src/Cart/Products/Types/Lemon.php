<?php
namespace Easydev\Vendor\ShoppingCart\Cart\Products\Types;

use Easydev\Vendor\ShoppingCart\Cart\Products\ProductType;

class Lemon extends ProductType
{
    const PRODUCT_NAME = 'Lemon';
}